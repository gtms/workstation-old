#! /usr/bin/env dshell
import 'dart:io';
import 'dart:async';

import 'package:dshell/dshell.dart';
import 'package:args/args.dart';

Future<void> main(List<String> args) async {
  var parser = new ArgParser();
  parser.addFlag('public', abbr: 'p', defaultsTo: true);
  parser.addOption('local', abbr: 'l');
  var results = parser.parse(args);

  if (results['local'] != null) {
    print(await getLocalIp(results['local']));
    return 0;
  }

  'dig +short myip.opendns.com @resolver1.opendns.com'.run;
}

Future<String> getLocalIp(String eth) async {
  return (await NetworkInterface.list())
      .firstWhere((interface) => interface.name == eth, orElse: () => null)
      ?.addresses
      ?.first
      ?.address;
}
