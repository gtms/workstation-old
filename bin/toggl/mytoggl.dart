#!/usr/bin/env dshell
import 'package:dshell/dshell.dart';

int main(List<String> args) {
  var argParser = ArgParser();
  argParser.addFlag('duration', abbr: 'd');
  argParser.addFlag('start', negatable: false);
  var result = argParser.parse(args);

  var dur = duration;

  if (result['duration']) {
    print(dur ?? '');
  }

  if (result['start']) {
    var projects = ('toggl projects ls' | 'sed \'1d\'').toList();
    var project = menu('Choose project', projects, format: (line) {
      var res = (line as String).trim().split(RegExp('\\s+'));
      return '${res[0]} [${res[1]}]';
    });

    var regex = RegExp(r'\s+(\d+)\s.*$');
    if (regex.hasMatch(project)) {
      var id = regex.firstMatch(project).group(1);
      'toggl start ${id}'.run;
    } else {
      printerr('Cannot parse ID from the project');
    }
  }

  return 0;
}

String get getLastId {
  var list = ('toggl ls -f \'id\'' | 'sed -n \'2,2p\'').toList();
  if (list.isNotEmpty == true) {
    return list.first;
  }
  return null;
}

String get duration {
  var list = ('toggl ls -f \'stop,duration\'' | 'sed -n \'1d;s/\\s*//g;s/running//p\'').toList();
  if (list.isNotEmpty == true) {
    return list.first;
  }
  return null;
}
