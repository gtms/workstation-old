#!/bin/bash

function audio_switch_output() {
	#Device name variable
	devicename=$1
	
	#change the default sink
	pacmd "set-default-sink "$devicename""
	
	#move all inputs to the new sink
	for app in $(pacmd list-sink-inputs | sed -n -e 's/index:[[:space:]]\([[:digit:]]\)/\1/p');
	do
		pacmd "move-sink-input $app "$devicename""
	done
}

function is_audio_output_active() {
	devicename=$1
	ret=0
	sinks=$(pacmd list-sink-inputs | awk '/sink:/ {print $NF}' | awk -F"[<>]" '{print $2}')
	

	if [ -z $sinks ]; then
		local ret=1
	else
		for sinkname in $sinks; do
			if [ $sinkname != $devicename ]; then
				local ret=1
				break
			fi
		done
	fi

	echo $ret
}
