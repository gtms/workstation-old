#! /usr/bin/env dshell
import 'package:dshell/dshell.dart';

void main(List<String> args) {
  "`grep '^Exec' $args[0] | tail -1 | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^\"//g' | sed 's/\" *\$//g'` &".run;
}
