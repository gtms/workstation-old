#!/usr/bin/env zsh

op list items | jq '.[] | .overview.title'
