#! /usr/bin/env dshell
import 'package:dshell/dshell.dart';

List<String> folders = (env('XDG_DATA_DIRS').split(':')..removeWhere((folder) => !exists('${folder}applications')))
    .map((folder) => '${folder}applications')
    .toList();

var cache = '${env('XDG_CACHE_HOME') ?? '$HOME/.cache'}/dmenu_app';

RegExp pattern = RegExp(r'n\[(?<name>.*)\]f\[(?<file>.*)\]');

void main(List<String> args) async {
  (r"sed -E 's/n\[(.*)\]f\[.*\]/\1/' " + "$cache" | 'dmenu ${args.join(' ')}').forEach((app) {
    try {
      if (app?.isNotEmpty == true) {
        var appName = ("sed -n '/$app/p' '$cache'" | r"sed -E 's/n\[.*\]f\[(.*)\]/\1/'").toList().first;
        "gtk-launch $appName".run;
      }
    } catch (_) {}
  });
}
