#! /usr/bin/env dshell
import 'dart:io';
import 'package:dshell/dshell.dart';

void main(List<String> args) {
  ('1pass' | 'dmenu  ${args.join(' ')}').forEach((name) => ('1pass -p $name' | 'xclip').run);
  exit(0);
}
