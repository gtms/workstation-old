#! /usr/bin/env dshell
import 'package:dshell/dshell.dart';

List<String> folders = (env('XDG_DATA_DIRS').split(':')..removeWhere((folder) => !exists(correctFolder(folder))))
    .map((folder) => correctFolder(folder))
    .toList();

String correctFolder(String folder) => folder.endsWith('\/') ? '${folder}applications' : '${folder}/applications';

void main(List<String> args) {
  var cache = '${env('XDG_CACHE_HOME') ?? '$HOME/.cache'}/dmenu_app';
  cache.truncate();

  for (String folder in folders) {
    'stest -fl ${folder}'.forEach((file) {
      String name = "sed -n -e '/^Name/s/Name=//p' '$folder/$file'".firstLine;
      if (name != null) cache.append('n[${name}]f[${file.replaceAll('.desktop', '')}]');
    });
  }
}
