#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

export PATH="$PATH:$HOME/.config/polybar/bin"

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if type "xrandr"; then
 for m in $(polybar --list-monitors | cut -d":" -f1); do
  # Launch bar1 and bar2
   MONITOR=$m polybar -c $HOME/.config/polybar/config top &
 done
else
   polybar -c $HOME/.config/polybar/config top &
fi

echo "Bars launched..."

