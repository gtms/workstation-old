--- Ghost layer
-- @module layer.ghost

local plug = require("core.plug")

local layer = {}

function layer.register_plugins()
  plug.add_plugin("raghur/vim-ghost", {["do"]=":GhostInstall"})
end

function layer.init_config()
end

return layer
