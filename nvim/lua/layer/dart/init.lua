--- Dart layer
-- @module layer.dart
local plug = require("core.plug")
local autocmd = require("core.autocmd")

local layer = {}

function layer.register_plugins() plug.add_plugin("dart-lang/dart-vim-plugin") end

function layer.init_config()
  local format = require("layer.format")

  vim.api.nvim_set_var("dart_style_guide", 2)
  vim.api.nvim_set_var("dart_html_in_string", true)

	if pcall(require, "nvim_lsp") then
		local lsp = require("layer.lsp")
		local nvim_lsp = require("nvim_lsp")
		lsp.register_server(nvim_lsp.dartls)
	end

  format.add("dart", "dartfmt", {["exe"] = "dartfmt", ["args"] = {"-l 120"}, ["stdin"] = 1})
  format.enable("dart", "dartfmt")
  autocmd.bind_filetype("dart", function()
		vim.api.nvim_win_set_option(0, "foldmethod", "syntax")
	end)
end

return layer
