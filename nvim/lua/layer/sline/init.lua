--- Status line layer

local autocmd = require("core.autocmd")

local layer = {}

function layer.register_plugins()
end

local function get_mode_highlight()
  local mode = vim.api.nvim_get_mode()["mode"]
  if string.sub(mode, 1, 1) == "i" then
    return "modeInsert"
  else
    return "modeNormal"
  end
end

local function current_mode()
  local mode = vim.api.nvim_get_mode()["mode"]

  local translate_table = {
    n  = "Normal",
    no = "Normal·Operator Pending",
    v  = "Visual",
    V  = "V·Line",
    s  = "Select",
    S  = "S·Line",
    i  = "Insert",
    ic  = "Insert",
    R  = "Replace",
    Rv = "V·Replace",
    c  = "Command",
    cv = "Vim Ex",
    ce = "Ex",
    r  = "Prompt",
    rm = "More",
    t  = "Terminal"
  }
  translate_table["^V"] = "V·Block"
  translate_table["^S"] = "S·Block"
  translate_table["r?"] = "Confimm"
  translate_table["!"] = "Shell"

  local translated_mode= translate_table[mode]
  if translate_table[mode] == nil then translated_mode= "NORMAL" end
  return string.upper(translated_mode)
end

local function onread(err, data)
  if err then
    print ("ERROR:" .. err)
  end

  if data then
    print ("DATA:" .. data)
  end
end

local function git_branch()
  local branch = vim.api.nvim_call_function("system", {"git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\\n'"})
  if branch > "0" then branch = "  "..branch else branch ='' end
  vim.api.nvim_buf_set_var(0, "gitbranch", branch)
end

local function read_only()
  local readonly = vim.api.nvim_buf_get_option(0, "readonly")
  local modifiable = vim.api.nvim_buf_get_option(0, "modifiable")
  if readonly == true or modifiable == false then
    return ''
  end
  return ''
end

local function encoding()
  local fenc = vim.api.nvim_buf_get_option(0, "fenc")
  local enc = vim.api.nvim_get_option("enc")
  if fenc ~= '' then
    return fenc
  end
  return enc
end

local function format()
  return vim.api.nvim_buf_get_option(0, "ff")
end

local function size()
  file = vim.api.nvim_buf_get_name(0)
  local bytes = vim.api.nvim_call_function("getfsize", {file})
  local size = bytes
  local unit = "B"

  if bytes >= 1024 then
    size = (bytes / 1024)
    unit = "KB"
  end
  if (bytes / 1024) >= 1000 then
    size = (bytes / 1024) / 1000
    unit = "MB"
  end

  return string.format("%.2f%s",tonumber(size), unit)
end

local function permission()
  local file = vim.api.nvim_buf_get_name(0)
  return vim.api.nvim_call_function("getfperm", {file})
end

local function filetype()
  return vim.api.nvim_buf_get_option(0, "filetype")
end

local function lsp()
  local clients = vim.lsp.buf_get_clients()
  local client_names = {}
  for _, v in pairs(clients) do
    table.insert(client_names, v.name)
  end

  if #client_names > 0 then
    return "[LSP] " .. table.concat(client_names, ", ")
  else
    return ""
  end
end

function ActiveStatusLine()
  local line = ""

  local modehighlight = get_mode_highlight()
  line = line .. "%#".. modehighlight .."# " ..current_mode() .." %0*"

  if vim.api.nvim_call_function("exists",{ "b:gitbranch" }) == 1 then
    line = line .. "%1* " .. vim.api.nvim_buf_get_var(0, "gitbranch") .. " "
  end

  line = line .. "%2* %m "
  line = line .. "%2* " .. lsp() .. " "
  line = line .. "%2* %="
  line = line .. "%7*"
  line = line .. "%2* " .. read_only() .." "
  line = line .. "%2* " .. filetype() .. " "
  line = line .. "%1* " .. encoding() .. " [" .. format() .."] "
  line = line .. "%1* " .. size() .. " "
  line = line .. "%1* " .. permission() .. " "
  line = line .. "%#".. modehighlight .. "#%3p%% %3l:%-3c"
  return line
end

function layer.init_config()
  local api = vim.api
  api.nvim_set_option("laststatus", 2) -- always show status line

  api.nvim_command("hi User1 guibg=#434c5e")
  api.nvim_command("hi User2 guibg=#3b4252")
  api.nvim_command("hi User2 guibg=#3b4252")
  api.nvim_command("hi statuslineInactive guibg=#3b4252 guifg=#88c0d0")

  api.nvim_command("hi modeNormal guibg=#81a1c1 guifg=#2e3440")
  api.nvim_command("hi modeInsert guibg=#a3be8c guifg=#2e3440")

  api.nvim_command([[
  function! ActiveStatusLine()
    return luaeval('ActiveStatusLine()')
  endfunction
  ]])

  autocmd.bind("WinEnter,BufEnter *", function()
    git_branch();
    api.nvim_win_set_option(0, "statusline", "%!ActiveStatusLine()")
  end)
  autocmd.bind("WinLeave,BufLeave *", function()
    api.nvim_win_set_option(0, "statusline", "%#statuslineInactive#%F")
  end)

  -- autocmd.bind("InsertLeave *", function()
  --   update_highlight(vim.api.nvim_get_mode()["mode"])
  -- end)

end

return layer

