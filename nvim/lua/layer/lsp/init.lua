--- Language server protocol support, courtesy of Neovim
-- @module layer.lsp

local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")
local autocmd = require("core.autocmd")
local plug = require("core.plug")


local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("neovim/nvim-lsp")
  plug.add_plugin("haorenW1025/completion-nvim")
  plug.add_plugin("haorenW1025/diagnostic-nvim")
end

local function user_stop_all_clients()
  local clients = vim.lsp.get_active_clients()

  if #clients > 0 then
    vim.lsp.stop_client(clients)
    for _, v in pairs(clients) do
      print("Stopped LSP client " .. v.name)
    end
  else
    print("No LSP clients are running") end
end

local function user_attach_client()
  local filetype = vim.bo[0].filetype

  local server = layer.filetype_servers[filetype]
  if server ~= nil then
    print("Attaching LSP client " .. server.name .. " to buffer")
    server.manager.try_add()
  else
    print("No LSP client registered for filetype " .. filetype)
  end
end

--- Configures vim and plugins for this layer
function layer.init_config()
  vim.api.nvim_set_var("completion_enable_in_comemnt", 1)
  -- vim.api.nvim_set_var("completion_auto_change_source", 1)
  -- vim.api.nvim_set_var("completion_enable_auto_signature", 0)
  vim.api.nvim_set_option("completeopt",  "menuone,noinsert,noselect")
  vim.api.nvim_command("set shortmess+=c")

  -- Completion list
  -- vim.api.nvim_set_var("completion_chain_complete_list", {
  --   {complete_items = { "lsp", "snippet" }},
  --   {mode = "file"},
  --   {mode = "<c-p>"},
  --   {mode = "<c-n>"},
  -- })

  -- Bind leader keys
  local lsp_prefix = "<leader>l"
  keybind.set_group_name(lsp_prefix, "LSP")
  keybind.bind_function(edit_mode.NORMAL, lsp_prefix .. "s", user_stop_all_clients, nil, "Stop all LSP clients")
  keybind.bind_function(edit_mode.NORMAL, lsp_prefix .. "a", user_attach_client, nil, "Attach LSP client to buffer")
  -- Syntax under cursor
  keybind.bind_command(edit_mode.NORMAL, "<leader>lc", "<cmd>echo synIDattr(synID(line('.'), col('.'), 1), 'name')<CR>", nil, "Get syntax under curosr")

  -- Trigger autocompletion
  keybind.bind_command(edit_mode.INSERT, "<c-space>", "completion#trigger_completion()", {noremap = true, silent = true, expr = true}, "Trigger completion")

  -- Tabbing
  keybind.bind_command(edit_mode.INSERT, "<tab>", "pumvisible() ? '<C-n>' : '<tab>'", { noremap = true, expr = true })
  keybind.bind_command(edit_mode.INSERT, "<S-tab>", "pumvisible() ? '<C-p>' : '<S-tab>'", { noremap = true, expr = true })
  -- autocmd.bind_complete_done(function()
  --   if vim.fn.pumvisible() == 0 then
  --     vim.cmd("pclose")
  --   end
  -- end)

  -- Switching completion list
  keybind.bind_command(edit_mode.INSERT, "<C-j>", "<cmd>lua require'source'.prevCompletion()<CR>", nil, "Previous completion source")
  keybind.bind_command(edit_mode.INSERT, "<C-k>", "<cmd>lua require'source'.nextCompletion()<CR>", nil, "next completion source")

  -- Show documentaiton
  vim.api.nvim_command([[
    function! ShowDocumentation()
      if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
      endif
    endfunction
  ]])
  keybind.bind_command(edit_mode.NORMAL, "K", "<cmd>call ShowDocumentation()<CR>", { noremap = true }, "Show documentation")

  -- Jumping to places
  autocmd.bind_filetype("*", function()
    local server = layer.filetype_servers[vim.bo.ft]
    if server ~= nil then
      keybind.buf_bind_command(edit_mode.NORMAL, "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", { noremap = true, silent = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", { noremap = true, silent = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", { noremap = true, silent = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "K", "<cmd>lua vim.lsp.buf.hover()<CR>", { noremap = true, silent = true })
      keybind.bind_command(edit_mode.NORMAL, "<c-p>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", { noremap = true })
      keybind.bind_command(edit_mode.INSERT, "<c-p>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", { noremap = true })
      keybind.bind_command(edit_mode.NORMAL, "gr", "<cmd>lua vim.lsp.buf.references()<CR>", { noremap = true }, "Find references")
      keybind.bind_command(edit_mode.NORMAL, "gn", "<cmd>lua vim.lsp.buf.rename()<CR>", { noremap = true }, "Rename")
      keybind.bind_command(edit_mode.NORMAL, "gs", "<cmd>lua vim.lsp.buf.document_symbol()<CR>", { noremap = true, silent = true }, "Document symbol list")
      keybind.bind_command(edit_mode.NORMAL, "<leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", { noremap = true, silent = true }, "Formatting")
    end
  end)

  keybind.bind_command(edit_mode.NORMAL, "]g", "<cmd>NextDiagnostic<CR>", { noremap = true, silent = true }, "Next diagnostic")
  keybind.bind_command(edit_mode.NORMAL, "[g", "<cmd>PrevDiagnostic<CR>", { noremap = true, silent = true }, "Previous diagnostic")
end

--- Maps filetypes to their server definitions
--
-- <br>
-- Eg: `["rust"] = nvim_lsp.rls`
--
-- <br>
-- See `nvim_lsp` for what a server definition looks like
layer.filetype_servers = {}

--- Register an LSP server
--
-- @param server An LSP server definition (in the format expected by `nvim_lsp`)
-- @param config The config for the server (in the format expected by `nvim_lsp`)
function layer.register_server(server, config)
  local completion = require("completion") -- From completion-nvim
  local diagnostic = require("diagnostic") -- From diagnostic-nvim

  config = config or {}
  config.on_attach = function()
    completion.on_attach()
    diagnostic.on_attach()
  end
  config = vim.tbl_extend("keep", config, server.document_config.default_config)

  server.setup(config)

  for _, v in pairs(config.filetypes) do
    layer.filetype_servers[v] = server
  end
end

return layer

