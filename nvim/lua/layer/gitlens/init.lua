--- GitLens layer
-- @module layer.gitlens

local api = vim.api
local autocmd = require("core.autocmd")

local layer = {}

function layer.register_plugins()
end

function layer.blame_virt_text()
  local ft = vim.fn.expand('%:h:t')
  if ft == '' then
    return
  end
  if ft == 'bin' then
    return
  end
  api.nvim_buf_clear_namespace(0, 2, 0 ,-1)
  local currFile = vim.fn.expand('%')
  local line = api.nvim_win_get_cursor(0)
  local blame = vim.fn.system(string.format('git blame -c -L %d,%d %s', line[1], line[1], currFile))
  local hash = vim.split(blame, '%s')[1]
  local cmd = string.format("git show %s ", hash).."--format='%an | %ar | %s'"
  local text
  if hash == '00000000' then
    text = "Not Committed Yes"
  else
    text = vim.fn.system(cmd)
    text = vim.split(text, '\n')[1]
    if text:find("fatal") then
      text = "Not Committed Yes"
    end
  end
  api.nvim_buf_set_virtual_text(0, 2, line[1] - 1, {{ text,'GitLens' }}, {})
end

function layer.clear_blame_virt_text()
  api.nvim_buf_clear_namespace(0, 2, 0, -1)
end

function layer.init_config()
  autocmd.bind("CursorHold *", function () layer.blame_virt_text() end)
  autocmd.bind("CursorMoved, CursorMovedI *", function () layer.clear_blame_virt_text() end)
  vim.api.nvim_command("hi! link GitLens comment")
end


return layer
