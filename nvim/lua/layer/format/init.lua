--- Neoformat layer
local plug = require("core.plug")
local autocmd = require("core.autocmd")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")

local layer = {}
-- [ name ] description [ default ] optional/required
-- ---------------------------------------------------
-- [ exe ]  the name the formatter executable in the path [ n/a ] required
-- [ args ] list of arguments [ [] ]  optional
-- [ replace ]  overwrite the file, instead of updating the buffer [ 0 ]  optional
-- [ stdin ]  send data to the stdin of the formatter [ 0 ] optional
-- [ stderr ] capture stderr output from formatter  [ 0 ] optional
-- [ no_append ]    do not append the path of the file to the formatter command,
--                  used when the path is in the middle of a command  [ 0 ] optional
-- [ env ]  list of environment variable definitions to be prepended to the formatter command [ [] ]  optional
-- [ valid_exit_codes ] list of valid exit codes for formatters
--                      who do not respect common unix practices  [ [0] ] optional
function layer.add(filetype, formatter, config)
  if type(filetype) == "table" then
    for _, v in pairs(filetype) do vim.api.nvim_set_var("neoformat_" .. v .. "_" .. formatter, config) end
  elseif type(filetype) == "string" then
    vim.api.nvim_set_var("neoformat_" .. filetype .. "_" .. formatter, config)
  end
end

function layer.enable(filetype, formatter)
  if type(filetype) == "table" then
    for _, v in pairs(filetype) do vim.api.nvim_set_var("neoformat_enabled_" .. v, {formatter}); end
  elseif type(filetype) == "string" then
    vim.api.nvim_set_var("neoformat_enabled_" .. filetype, {formatter});
  end

  autocmd.bind_filetype(filetype, function()
    keybind.bind_command(edit_mode.NORMAL, "gq", "<cmd>Neoformat<CR>", {noremap = true}, "Format code")
  end)
end

function layer.register_plugins() plug.add_plugin("sbdchd/neoformat") end

function layer.init_config()
  vim.api.nvim_set_var("neoformat_basic_format_trim", 1)
  vim.api.nvim_set_var("neoformat_basic_format_retab", 1)
end

return layer
