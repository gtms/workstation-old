--- Snippets layer
-- @module layer.snippets

local plug = require("core.plug")

local layer = {}

function layer.register_plugins()
  plug.add_plugin("sirver/Ultisnips")
  plug.add_plugin("honza/vim-snippets")
end

function layer.init_config()
  vim.api.nvim_set_var("completion_enable_snippet", "UltiSnips")
  vim.api.nvim_set_var("UltiSnipsExpandTrigger", "<nop>")
  vim.api.nvim_set_var("UltiSnipsJumpForwardTrigger", "<c-l>")
  vim.api.nvim_set_var("UltiSnipsJumpBackwardTrigger", "<c-h>")
end

return layer
