--- C++ layer
local autocmd = require("core.autocmd")

local layer = {}

function layer.register_plugins() end

function layer.init_config()
	local format = require("layer.format")


	local cpp_fts = {"cpp","h"}
	format.add(cpp_fts, "clangformat", {
            ["exe"] = " clang-format",
            ["args"] = { "-assume-filename=" .. vim.fn["expand"]("%:t"),  "--style=file" },
            ["stdin"] = 1,
	})
  format.enable(cpp_fts, "clangformat")

  autocmd.bind_filetype({"cpp", "h"}, function() vim.api.nvim_set_option("foldmethod", "syntax") end)
end

return layer
