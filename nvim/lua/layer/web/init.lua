--- Web development layer
local plug = require("core.plug")
local autocmd = require("core.autocmd")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")
local format = require("layer.format")

local layer = {}

local function init_html_latte(html_fts)
  -- format
  format.add(html_fts, "htmlbeautify", {
    ["exe"] = 'html-beautify',
    ["args"] = {'--indent-size ' .. vim.fn["shiftwidth"](), '-w 120', '--wrap-attributes aligned-multiple'},
    ["stdin"] = 1
  })
  format.enable(html_fts, "htmlbeautify")

  autocmd.bind_filetype(html_fts, function() vim.api.nvim_set_option("shiftwidth", 2) end)

  -- html/latte
  autocmd.bind_filetype({"latte", "html.latte"}, function()
    vim.api.nvim_buf_set_option(0, "filetype", "latte")
    vim.api.nvim_command("call tcomment#type#Define('latte', '{* %s *}')")
    if vim.fn["exists"]("loaded_matchit") == 1 then
      vim.api.nvim_buf_set_var(0, "match_ignorecase", 1)
      vim.api.nvim_buf_set_var(0, "match_words", [[<:>,]] .. [[<\@<=[ou]l\>[^>]*\%(>\|$\):<\@<=li\>:<\@<=/[ou]l>,]] ..
                                   [[<\@<=dl\>[^>]*\%(>\|$\):<\@<=d[td]\>:<\@<=/dl>,]] ..
                                   [[<\@<=\([^/][^ \t>]*\)[^>]*\%(>\|$\):<\@<=/\1>]])
    end
  end)
end

local function init_neon()
  autocmd.bind("BufRead,BufNewFile *.neon", function() vim.api.nvim_buf_set_option(0, "filetype", "yaml") end)
end

local function init_scss()
  autocmd.bind_filetype("scss", function()
    vim.api.nvim_set_var("neoformat_enabled_scss", {"prettier"})
    keybind.bind_command(edit_mode.NORMAL, "<leader>f", "<cmd>Neoformat<CR>", {noremap = true, silent = true}, "Format")
  end)
end

function layer.register_plugins()
  plug.add_plugin("mattn/emmet-vim", {["for"] = {"php", "html", "css", "less", "latte"}})
  plug.add_plugin("vim-php/vim-composer")
  plug.add_plugin("fpob/nette.vim")
end

function layer.init_config()
  local html_fts = {"html", "latte"}

  -- composer
  vim.api.nvim_set_var("composer_cmd", "composer")

  if pcall(require, "nvim_lsp") then
    local lsp = require("layer.lsp")
    local nvim_lsp = require("nvim_lsp")
    lsp.register_server(nvim_lsp.tsserver)
    lsp.register_server(nvim_lsp.cssls)
    lsp.register_server(nvim_lsp.intelephense, {
      init_options = {
        licenceKey = os.getenv("XDG_CONFIG_HOME") .. "/intelephense/licenceKey",
        globalStoragePath = os.getenv("XDG_CONFIG_HOME") .. "/intelephense"
      }
    })
    lsp.register_server(nvim_lsp.html, {filetypes = html_fts})
  end

  init_html_latte(html_fts)
  init_neon()
  init_scss()
end

return layer
