--- Lua layer
-- @module layer.lua
local autocmd = require("core.autocmd")

local layer = {}

local function on_filetype_lua()
  vim.api.nvim_buf_set_option(0, "shiftwidth", 2)
  vim.api.nvim_buf_set_option(0, "tabstop", 2)
  vim.api.nvim_buf_set_option(0, "softtabstop", 4)
end

--- Returns plugins required for this layer
function layer.register_plugins() end

--- Configures vim and plugins for this layer
function layer.init_config()
  local format = require("layer.format")

  if pcall(require, "nvim_lsp") then
    local lsp = require("layer.lsp")
    local nvim_lsp = require("nvim_lsp")
		lsp.register_server(nvim_lsp.sumneko_lua, {settings = {Lua = {diagnostics = {globals = {"vim"}}}}})
	else

	end

  format.add("lua", "luaformatter", {["exe"] = "lua-format", ["args"] = {"--config=/home/tms/.config/lua/lua-format"}})
  format.enable("lua", "luaformatter")
  autocmd.bind_filetype("lua", on_filetype_lua)
end

return layer

