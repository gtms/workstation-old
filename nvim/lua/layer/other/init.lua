--- Layer for other stuff
local plug = require("core.plug")
local autocmd = require("core.autocmd")

local layer = {}

function layer.register_plugins() plug.add_plugin("tikhomirov/vim-glsl", {["for"] = "glsl"}) end

function layer.init_config()
  -- glsl
  autocmd.bind("BufNewFile,BufRead *.vs,*.fs", function() vim.api.nvim_buf_set_var(0, "filetype", "glsl") end)

  if pcall(require, "nvim_lsp") then
    local lsp = require("layer.lsp")
    local nvim_lsp = require("nvim_lsp")
    lsp.register_server(nvim_lsp.vimls)
    lsp.register_server(nvim_lsp.bashls)
    lsp.register_server(nvim_lsp.jsonls)
  end

  -- i3config
  autocmd.bind_filetype("i3config", function() vim.api.nvim_buf_set_option(0, "commentstring", "# %s") end)
  autocmd.bind_filetype("json", function() vim.api.nvim_command("match Comment +\\/\\/.\\+$+") end)
end

return layer
