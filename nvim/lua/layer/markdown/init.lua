--- Layer for other stuff

local plug = require("core.plug")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")

local layer = {}

function layer.register_plugins()
  plug.add_plugin("suan/vim-instant-markdown", {["for"] = "markdown"})
end

function layer.init_config()
  vim.api.nvim_set_var("instant_markdown_autostart", 0)
  vim.api.nvim_set_var("instant_markdown_autoscroll", 1)

  keybind.bind_command(edit_mode.NORMAL, "gmd", "<cmd>InstantMarkdownPreview<CR>", { noremap = true }, "Preview markdown")
  keybind.bind_command(edit_mode.NORMAL, "gmdq", "<cmd>InstantMarkdownStop<CR>", { noremap = true }, "Stop preview markdown")

-- "let g:instant_markdown_slow = 1
-- "let g:instant_markdown_autostart = 0
-- "let g:instant_markdown_open_to_the_world = 1
-- "let g:instant_markdown_allow_unsafe_content = 1
-- "let g:instant_markdown_allow_external_content = 0
-- "let g:instant_markdown_mathjax = 1
-- "let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
-- "let g:instant_markdown_autoscroll = 0
-- "let g:instant_markdown_port = 8888
-- "let g:instant_markdown_python = 1
end

return layer
