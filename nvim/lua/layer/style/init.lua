--- The styling layer
-- @module layer.style
local layer = {}

local plug = require("core.plug")
local autocmd = require("core.autocmd")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")
local api = vim.api

--- Returns plugins required for this layer
function layer.register_plugins()
  -- plug.add_plugin("arcticicestudio/nord-vim") -- Colorscheme
  plug.add_plugin('Yggdroot/indentLine') -- Indent guides
  plug.add_plugin("camspiers/animate.vim")
  plug.add_plugin("vim-scripts/colorizer")
end

--- Configures vim and plugins for this layer function layer.init_config()
function layer.init_config()
  api.nvim_set_option("termguicolors", true)
  api.nvim_command("colorscheme nord")
  api.nvim_command("filetype plugin on")

  api.nvim_win_set_option(0, "colorcolumn", "120")
  api.nvim_win_set_option(0, "cursorline", true)
  api.nvim_win_set_option(0, "signcolumn", "yes")
  api.nvim_win_set_option(0, "number", true)
  api.nvim_win_set_option(0, "relativenumber", true)
  api.nvim_buf_set_option(0, "shiftwidth", 2)
  api.nvim_buf_set_option(0, "expandtab", true) -- Use spaces instead of tabs
  api.nvim_buf_set_option(0, "autoindent", true)
  api.nvim_buf_set_option(0, "smartindent", true)
  api.nvim_set_option("list", false)
  api.nvim_set_option("listchars", "space:·,tab:» ,extends:›,precedes:‹,nbsp:_,trail:_,eol:$")
  api.nvim_set_option("ignorecase", true)
  api.nvim_set_option("smartcase", true)
  api.nvim_set_option("splitright", true)
  api.nvim_set_option("incsearch", true)
  api.nvim_set_option("inccommand", "nosplit")
  api.nvim_set_option("hlsearch", true)
  api.nvim_set_option("showmode", false)
  api.nvim_set_option("showbreak", ">\\ ")
  api.nvim_set_option("scrolloff", 5)
  api.nvim_set_option("sidescrolloff", 5)
  api.nvim_set_option("mouse", "a")
  api.nvim_set_option("swapfile", false)
  api.nvim_win_set_option(0, "foldmethod", "indent")
  api.nvim_set_option("foldlevelstart", 99)
  api.nvim_set_option("backup", false)
  api.nvim_set_option("writebackup", false)
  api.nvim_set_option("updatetime", 100)
  api.nvim_set_option("hidden", true)

  -- trailing space show as error
  api.nvim_command('match ErrorMsg "\\s\\+$"')

  -- animate
  api.nvim_command("let g:animate#duration = 100.0")
  keybind.bind_command(edit_mode.NORMAL, "<Up>", ":call animate#window_delta_height(10)<CR>")
  keybind.bind_command(edit_mode.NORMAL, "<Down>", ":call animate#window_delta_height(-10)<CR>")
  keybind.bind_command(edit_mode.NORMAL, "<Left>", ":call animate#window_delta_width(-10)<CR>")
  keybind.bind_command(edit_mode.NORMAL, "<Right>", ":call animate#window_delta_width(10)<CR>")

  -- Configure indent guides
  api.nvim_set_var("indentLine_color_gui", "#3b4252")
  api.nvim_set_var("indentLine_char_list", {"▏"})
  autocmd.bind_filetype('help', function() api.nvim_command("IndentLinesDisable") end)

  api.nvim_command("hi MatchParen guifg=#ECEFF4 guibg=#5E81AC")
  api.nvim_command("hi HighlightedLineNr guifg=#D8DEE9 ctermfg=7")
  api.nvim_command("hi CursorLineNr guifg=#ECEFF4 guibg=#2E3440 ctermfg=5")
  api.nvim_command("hi Folded guifg=#5e81a1 guibg=#2e3440")
end

return layer
