--- File management
-- @module layer.file_man
local plug = require("core.plug")
local kb = require("core.keybind")
local autocmd = require("core.autocmd")
local em = require("core.edit_mode")

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("junegunn/fzf", {["dir"] = "$HOME/lib/fzf", ["do"] = "./install -all"})
  plug.add_plugin("junegunn/fzf.vim")
  plug.add_plugin("pechorin/any-jump.vim")
  plug.add_plugin("Shougo/neomru.vim")
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- fzf
  vim.api.nvim_set_var("fzf_command_prefix", "Fzf")
  -- vim.g.fzf_preview_command = "bat --color=always --style=grid {-1}"
  -- vim.g.fzf_layout = {window = 'call fzf_preview#window#create_centered_floating_window()'}
  -- vim.g.fzf_colors = {pointer = {'fg', '#00ff00'}}
  local fzf_prefix = "<space>"
  local fzf_opts = {noremap = true, silent = true}
  kb.set_group_name(fzf_prefix, "FZF")
  kb.bind_command(em.NORMAL, fzf_prefix .. "d", "<cmd>FzfFiles<CR>", fzf_opts, "Project directories")
  kb.bind_command(em.NORMAL, fzf_prefix .. "g", "<cmd>FzfGFiles<CR>", fzf_opts, "Git files")
  kb.bind_command(em.NORMAL, fzf_prefix .. "b", "<cmd>FzfBuffers<CR>", fzf_opts, "Buffers")
  kb.bind_command(em.NORMAL, fzf_prefix .. "l", "<cmd>FzfBLines!<CR>", fzf_opts, "Buffer lines")
  kb.bind_command(em.NORMAL, fzf_prefix .. "t", "<cmd>FzfBTags<CR>", fzf_opts, "Buffer tags")
  kb.bind_command(em.NORMAL, fzf_prefix .. "r", "<cmd>FzfAg!<CR>", fzf_opts, "Silver searcher")
  kb.bind_command(em.NORMAL, fzf_prefix .. "s", "<cmd>FzfSnippets<CR>", fzf_opts, "Snippets")

  -- anyjump
  kb.bind_command(em.NORMAL, "gj", "<cmd>AnyJump<CR>", {noremap = true, silent = true}, "Any jump")
  vim.api.nvim_set_var("any_jump_disable_default_keybindings", true)
  vim.api.nvim_set_var("any_jump_colors", {["preview"] = ""})

end

return layer
