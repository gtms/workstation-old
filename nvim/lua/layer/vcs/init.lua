--- The version control layer
-- @module layer.vcs

local layer = {}

local plug = require("core.plug")
local autocmd = require("core.autocmd")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("airblade/vim-gitgutter") -- VCS diffs in the gutter
  plug.add_plugin("tpope/vim-fugitive")
  plug.add_plugin("shumphrey/fugitive-gitlab.vim")
end

local cmd_is_set = false;
local function quickfix_hunks()
  vim.api.nvim_command(":GitGutterQuickFix")
  local list = vim.fn.getqflist({["size"] = 0})
  print(cmd_is_set)
  if list.size == 0 then
    print('[GitGutter] No saved hunks')
    vim.api.nvim_command("cclose")
    if cmd_is_set == true then
      autocmd.delete("User GitGutter")
      cmd_is_set = false
    end
  else
    if cmd_is_set then
      vim.api.nvim_command("copen | wincmd p")
    else
      vim.api.nvim_command("copen")
    end
    if cmd_is_set == false then
      autocmd.bind("User GitGutter", function()
        quickfix_hunks()
      end)
      cmd_is_set = true
    end
  end
end

--- Configures vim and plugins for this layer

function layer.init_config()
  -- gitgutter
  vim.api.nvim_set_var("gitgutter_map_keys", 0)
  vim.api.nvim_set_var("gitgutter_sign_added", "▏ ")
  vim.api.nvim_set_var("gitgutter_sign_modified", "▏ ")
  vim.api.nvim_set_var("gitgutter_sign_removed", "▏ ")
  vim.api.nvim_set_var("gitgutter_sign_modified_removed", "▏-")
  local gitgutter_prefix = "gh"
  keybind.bind_command(edit_mode.NORMAL, gitgutter_prefix.."u", "<cmd>GitGutterUndoHunk<CR>", nil, "Undo hunk")
  keybind.bind_command(edit_mode.NORMAL, gitgutter_prefix.."p", "<cmd>GitGutterPreviewHunk<CR>", nil, "Preview hunk")
  keybind.bind_command(edit_mode.NORMAL, gitgutter_prefix.."s", "<cmd>GitGutterStageHunk<CR>", nil, "Stage hunk")
  keybind.bind_command(edit_mode.NORMAL, gitgutter_prefix.."f", "<cmd>GitGutterFold<CR>", nil, "Fold unchanged")
  keybind.bind_function(edit_mode.NORMAL, gitgutter_prefix.."q", quickfix_hunks, { silent = true }, "Quickfix hunks")
  keybind.bind_command(edit_mode.NORMAL, "]c", "<cmd>GitGutterNextHunk<CR>", nil, "Next hunk")
  keybind.bind_command(edit_mode.NORMAL, "[c", "<cmd>GitGutterPrevHunk<CR>", nil, "Prev hunk")
end

return layer
