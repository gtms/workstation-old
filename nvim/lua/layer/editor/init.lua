--- User-specific editor tweaks
-- @module layer.editor
local api = vim.api
local plug = require("core.plug")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("tpope/vim-surround")
  plug.add_plugin("tpope/vim-unimpaired")
  plug.add_plugin("michaeljsmith/vim-indent-object")
  plug.add_plugin("jiangmiao/auto-pairs")
  plug.add_plugin("tomtom/tcomment_vim")
  plug.add_plugin("Yggdroot/hiPairs")
  plug.add_plugin("skwp/greplace.vim")
  plug.add_plugin("kkoomen/vim-doge")
end

--- Configures vim and plugins for this layer
function layer.init_config()
  vim.g.mapleader = "\\"
  vim.g.maplocalleader = "\\"

  -- highlight scope
  vim.api.nvim_set_var("loaded_matchparen", 1)
  vim.api.nvim_set_var("hiPairs_stopline_more", 500)
  vim.api.nvim_set_var("hiPairs_hl_matchPair", {
    ["term"] = "underline,bold",
    ["cterm"] = "bold",
    ["ctermfg"] = "0",
    ["ctermbg"] = "180",
    ["gui"] = "bold",
    ["guifg"] = "White",
    ["guibg"] = "#4c556a"
  })
  vim.api.nvim_set_var("hiPairs_hl_unmatchPair", {
    ["term"] = "underline,bold",
    ["cterm"] = "bold",
    ["ctermfg"] = "0",
    ["ctermbg"] = "180",
    ["gui"] = "bold",
    ["guifg"] = "#fb616a",
    ["guibg"] = "#4c556a"
  })

  -- window movement
  keybind.bind_command({edit_mode.NORMAL, edit_mode.INSERT}, "<c-h>", "<c-w>h", {noremap = true}, "Move cursor left")
  keybind.bind_command(edit_mode.NORMAL, "<c-l>", "<c-w>l", {noremap = true}, "Move cursor right")
  keybind.bind_command(edit_mode.NORMAL, "<c-j>", "<c-w>j", {noremap = true}, "Move cursor down")
  keybind.bind_command(edit_mode.NORMAL, "<c-k>", "<c-w>k", {noremap = true}, "Move cursor up")

  -- copy / paste
  keybind.bind_command(edit_mode.VISUAL, "<c-c>", "\"*y", {noremap = true}, "Copy to system clipboard")
  keybind.bind_command(edit_mode.NORMAL, "cp", "\"*y", {noremap = true}, "Copy to system clipboard")
  keybind.bind_command(edit_mode.NORMAL, "cpp", "\"*yy", {noremap = true}, "Copy line to system clipboard")

  -- remap double usage
  keybind.bind_command(edit_mode.NORMAL, "d.", "dd", {noremap = true}, "Delete line")
  keybind.bind_command(edit_mode.NORMAL, "y.", "yy", {noremap = true}, "Yank line")
  keybind.bind_command(edit_mode.NORMAL, "c.", "cc", {noremap = true}, "Change line")
  keybind.bind_command(edit_mode.NORMAL, "g.", "gg", {noremap = true}, "First line")
  keybind.bind_command(edit_mode.NORMAL, "v.", "V", {noremap = true}, "Select line")

  -- shortcuts for vim conf ig
  vim.api.nvim_command("command! Reload execute 'source $MYVIMRC'")
  vim.api.nvim_command("command! Config execute ':edit $MYVIMRC'")
  vim.api.nvim_command("command! Update execute ':source $MYVIMRC|PlugUpgrade|PlugClean|PlugUpdate|source $MYVIMRC'")

  -- sort
  api.nvim_command([[
    fun! SortLines(type) abort
      '[,']sort i
    endfun
  ]])
  keybind.bind_command(edit_mode.VISUAL, "gs", ":sort i<CR>", {noremap = true, silent = true}, "Sort ")
  keybind.bind_command(edit_mode.NORMAL, "gs", ":set opfunc=SortLines<CR>g@", {noremap = true, silent = true}, "Sort ")
end

return layer
