--- Asynctasks layer
-- @module layer.dart

local plug = require("core.plug")
local keybind = require("core.keybind")
local edit_mode = require("core.edit_mode")

local layer = {}

function layer.register_plugins()
  plug.add_plugin('skywind3000/asynctasks.vim')
  plug.add_plugin('skywind3000/asyncrun.vim')
end

function layer.init_config()
  local api = vim.api
api.nvim_set_var("asynctasks_term_rows", 10)
api.nvim_set_var("asynctasks_term_focus", 0)
api.nvim_set_var("asynctasks_term_reuse", 1)
api.nvim_set_var("asynctasks_confirm", 0)
api.nvim_set_var("asynctasks_extra_config", {'~/.config/tasks/tasks.ini'})

keybind.bind_command(edit_mode.NORMAL, "<f9>", "<cmd>AsyncTask project-build<CR>", { noremap = true, silent = true }, "Build a project")
keybind.bind_command(edit_mode.NORMAL, "<f10>", "<cmd>AsyncTask project-run<CR>", { noremap = true, silent = true }, "Run a project")
keybind.bind_command(edit_mode.NORMAL, "confa", "<cmd>AsyncTaskEdit<CR>", { noremap = true, silent = true }, "Edit asynctasks config")

end

return layer
