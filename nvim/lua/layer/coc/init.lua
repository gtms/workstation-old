--- Coc intellises layer
-- @module layer.coc
local plug = require("core.plug")
local kb = require("core.keybind")
local em = require("core.edit_mode")
local acmd = require("core.autocmd")

local layer = {}

function layer.register_plugins() plug.add_plugin("neoclide/coc.nvim", {["branch"] = "release"}) end

function layer.init_config()
  vim.api.nvim_command([[
  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
  endfunction
	]])
  kb.bind_command(em.INSERT, "<TAB>", "pumvisible() ? '<C-n>' : <SID>check_back_space() ? '<TAB>' : coc#refresh()",
                  {noremap = true, silent = true, expr = true})
  kb.bind_command(em.INSERT, "<S-TAB>", "pumvisible() ? '<C-n>' : '<C-h>'", {noremap = true, expr = true})

  kb.bind_command(em.INSERT, "<C-space>", "coc#refresh()", {noremap = true, silent = true, expr = true},
                  "Trigger autocompletion")

  -- " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
  -- " position. Coc only does snippet and additional edit on confirm.
  if vim.fn['exists']('*complete_info') == 1 then
    kb.bind_command(em.INSERT, "<CR>", "complete_info()['selected'] != '-1' ? '<C-y>' : '<C-g>u<CR>'",
                    {noremap = true, expr = true})
  else
    kb.bind_command(em.INSERT, "<CR>", "pumvisible() ? '<C-y>' : '<C-g>u<CR>'", {expr = true})
  end

  -- " Use `[g` and `]g` to navigate diagnostics
  kb.bind_command(em.NORMAL, "]g", "<Plug>(coc-diagnostic-next)", {silent = true}, "Next diagnostic")
  kb.bind_command(em.NORMAL, "[g", "<Plug>(coc-diagnostic-prev)", {silent = true}, "Previous diagnostic")

  -- " GoTo code navigation.
  kb.bind_command(em.NORMAL, "gd", "<Plug>(coc-definition)", {silent = true}, "Go to definition")
  kb.bind_command(em.NORMAL, "gD", "<Plug>(coc-type-definition)", {silent = true}, "Go to type definition")
  kb.bind_command(em.NORMAL, "gr", "<Plug>(coc-references)", {silent = true}, "Find references")
  kb.bind_command(em.NORMAL, "gi", "<Plug>(coc-implementation)", {silent = true}, "Go to implementation")

  -- " Use K to show documentation in preview window.
  vim.api.nvim_command([[
	function! s:show_documentation()
		if (index(['vim','help'], &filetype) >= 0)
			execute 'h '.expand('<cword>')
		else
			call CocAction('doHover')
		endif
	endfunction
		]])
  kb.bind_command(em.NORMAL, "K", "<cmd>call <SID>show_documentation()<CR>", {noremap = true, silent = true},
                      "Show documentation")
  kb.bind_command(em.NORMAL, "gn", "<Plug>(coc-rename)", nil, "Rename")
  -- kb.bind_command(em.NORMAL, "<c-p>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", {noremap = true})
  -- kb.bind_command(em.INSERT, "<c-p>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", {noremap = true})
  -- kb.bind_command(em.NORMAL, "gs", "<cmd>lua vim.lsp.buf.document_symbol()<CR>", {noremap = true, silent = true},
  --                 "Document symbol list")
  -- kb.bind_command(em.NORMAL, "<leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", {noremap = true, silent = true},
  -- "Formatting")

  -- " Highlight the symbol and its references when holding the cursor.
  -- acmd.bind_cursor_hold(function() vim.api.nvim_command("CocActionAsync('highlight')") end)

  acmd.bind("User CocJumpPlaceholder", function() vim.api.nvim_command("CocActionAsync('showSignatureHelp')") end)

  -- " Applying codeAction to the selected region.
  -- " Example: `<leader>aap` for current paragraph
  kb.bind_command(em.VISUAL, "<leader>a", "<Plug>(coc-codeaction-selected)", nil, "Region code action")
  kb.bind_command(em.NORMAL, "<leader>a", "<Plug>(coc-codeaction-selected)", nil, "Region Code action")
  -- " Remap keys for applying codeAction to the current line.
  kb.bind_command(em.NORMAL, "<leader>ac", "<Plug>(coc-codeaction)", nil)
  -- " Apply AutoFix to problem on the current line.
  kb.bind_command(em.NORMAL, "<leader>qf", "<Plug>(coc-fix-current)", nil, "AutoFix")
  --
  -- " Introduce function text object
  -- " NOTE: Requires 'textDocument.documentSymbol' support from the language server.
  -- xmap if <Plug>(coc-funcobj-i)
  -- xmap af <Plug>(coc-funcobj-a)
  -- omap if <Plug>(coc-funcobj-i)
  -- omap af <Plug>(coc-funcobj-a)
  --
  -- " Use <TAB> for selections ranges.
  -- " NOTE: Requires 'textDocument/selectionRange' support from the language server.
  -- " coc-tsserver, coc-python are the examples of servers that support it.
  -- nmap <silent> <TAB> <Plug>(coc-range-select)
  -- xmap <silent> <TAB> <Plug>(coc-range-select)
  --
  --
  -- " Add (Neo)Vim's native statusline support.
  -- " NOTE: Please see `:h coc-status` for integrations with external plugins that
  -- " provide custom statusline: lightline.vim, vim-airline.
  -- set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
  --
  -- " Mappings using CoCList:
  -- " Show all diagnostics.
  -- nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
  -- " Manage extensions.
  -- nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
  -- " Show commands.
  -- nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
  -- " Find symbol of current document.
  -- nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
  -- " Search workspace symbols.
  -- nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
  -- " Do default action for next item.
  -- nnoremap <silent> <space>j  :<C-u>CocNext<CR>
  -- " Do default action for previous item.
  -- nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
  -- " Resume latest coc list.
  -- nnoremap <silent> <space>p  :<C-u>CocListResume<CR>e
end

return layer
