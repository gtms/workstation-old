local reload = require("core.reload")
reload.unload_user_modules()

local log = require("core.log")
log.init()

local layer = require("core.layer")
local keybind = require("core.keybind")
local autocmd = require("core.autocmd")

keybind.register_plugins()
autocmd.init()

local native_lsp = true
vim.api.nvim_set_var("native_lsp", native_lsp)

layer.add_layer("layer.editor")
layer.add_layer("layer.style")
layer.add_layer("layer.format")
layer.add_layer("layer.sline")
layer.add_layer("layer.file_man")
layer.add_layer("layer.vcs")
layer.add_layer("layer.asynctasks")
layer.add_layer("layer.snippets")
-- layer.add_layer("layer.gitlens")
-- lang layers
if native_lsp then
  layer.add_layer("layer.lsp")
else
  layer.add_layer("layer.coc")
end
layer.add_layer("layer.lua")
layer.add_layer("layer.dart")
layer.add_layer("layer.web")
layer.add_layer("layer.cpp")
-- /lang layers
layer.add_layer("layer.markdown")
layer.add_layer("layer.other")
layer.finish_layer_registration()

keybind.post_init()
